extern crate serde;
extern crate serde_json;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn to_struct_string(json: &str) -> String {
    from_json(json).join("")
}

use std::collections::HashMap;

use serde_json::Map;

#[derive(Debug, Clone)]
pub struct Struct {
    pub name: String,
    pub entries: Vec<Entry>,
}

#[derive(Debug, Clone)]
pub struct Entry {
    pub name: String,
    pub type_name: String,
}

pub fn first_to_upper(str: String) -> String {
    let chars = str.chars().collect::<Vec<char>>();
    let (h, t) = chars.split_at(1);
    [h[0].to_ascii_uppercase()]
        .iter()
        .chain(t)
        .collect::<String>()
}

impl Struct {
    pub fn make_string(self) -> String {
        let mut s = format!("pub struct {} {{\n", self.name);
        self.entries.iter().for_each(|x| {
            s.push_str(format!("    {}: {},\n", x.name, x.type_name).as_str());
        });
        s.push_str("}\n\n");
        s
    }
}

pub fn value_router(k: String, v: serde_json::Value) -> Entry {
    match v {
        serde_json::Value::Number(x) if x.is_f64() => Entry {
            name: k,
            type_name: "f64".to_string(),
        },
        serde_json::Value::Number(x) if x.is_i64() => Entry {
            name: k,
            type_name: "i64".to_string(),
        },
        serde_json::Value::Number(x) if x.is_u64() => Entry {
            name: k,
            type_name: "u64".to_string(),
        },
        serde_json::Value::Bool(_x) => Entry {
            name: k,
            type_name: "bool".to_string(),
        },
        serde_json::Value::Array(x) => {
            if x.is_empty() {
                Entry {
                    name: k,
                    type_name: "Vec<unknown>".to_string(),
                }
            } else {
                let sub_entry = value_router(k.clone(), x[0].clone());
                Entry {
                    name: k,
                    type_name: format!("Vec<{}>", sub_entry.type_name),
                }
            }
        }
        serde_json::Value::String(_x) => Entry {
            name: k,
            type_name: "String".to_string(),
        },
        serde_json::Value::Null => Entry {
            name: k,
            type_name: "Option<unknown>".to_string(),
        },
        serde_json::Value::Object(_x) => {
            let type_name_upper = first_to_upper(k.to_string());
            Entry {
                name: k,
                type_name: type_name_upper,
            }
        }
        _ => Entry {
            name: k,
            type_name: "unknown".to_string(),
        },
    }
}

pub fn entry_router(key_values: HashMap<String, serde_json::Value>) -> Vec<Entry> {
    key_values
        .into_iter()
        .map(|(k, v)| value_router(k, v))
        .collect::<Vec<Entry>>()
}

pub fn entry_router_map(key_values: Map<String, serde_json::Value>) -> Vec<Entry> {
    key_values
        .into_iter()
        .map(|(k, v)| value_router(k, v))
        .collect::<Vec<Entry>>()
}

pub fn object_extractor(k: String, v: serde_json::Value) -> Option<(String, serde_json::Value)> {
    match v {
        serde_json::Value::Object(_) => Some((k, v)),
        serde_json::Value::Array(vs) => {
            if vs.is_empty() {
                None
            } else {
                object_extractor(k, vs[0].clone())
            }
        }
        _ => None,
    }
}

pub fn convert_entity_rec(key_values: Map<String, serde_json::Value>, name: String) -> Vec<Struct> {
    let entries = entry_router_map(key_values.clone());
    let mut struct_result = vec![Struct { name, entries }];

    let objects_struct = key_values
        .iter()
        .filter_map(|(k, v)| object_extractor(k.clone(), v.clone()))
        .flat_map(|(k, v)| convert_entity_rec(v.as_object().unwrap().clone(), first_to_upper(k)))
        .collect::<Vec<_>>();

    struct_result.extend(objects_struct);
    struct_result
}

pub fn convert_entity(key_values: HashMap<String, serde_json::Value>, name: String) -> Vec<Struct> {
    let entries = entry_router(key_values.clone());
    let mut struct_result = vec![Struct { name, entries }];

    let objects_struct = key_values
        .iter()
        .filter_map(|(k, v)| object_extractor(k.clone(), v.clone()))
        .flat_map(|(k, v)| convert_entity_rec(v.as_object().unwrap().clone(), first_to_upper(k)))
        .collect::<Vec<_>>();

    struct_result.extend(objects_struct);
    struct_result
}

pub fn from_json(json: &str) -> Vec<String> {
    let key_values: HashMap<String, serde_json::Value> = serde_json::from_str(json).unwrap();
    let struct_results = convert_entity(key_values, "RootObject".to_string());
    struct_results
        .iter()
        .map(|x| x.clone().make_string())
        .collect()
}

// pub fn to_struct(json: HashMap<String, serde_json::Value>) -> Vec<String> {}

#[cfg(test)]
mod tests {
    use super::{first_to_upper, from_json};

    #[test]
    fn first_to_upper_test() {
        assert_eq!(first_to_upper("abc".to_string()), "Abc");
    }

    #[test]
    fn to_json_test() {
        let json = r#"
{"menu": {  
  "id": "file",  
  "value": "File",  
  "popup": {  
    "menuitem": [  
      {"value": "New", "onclick": "CreateDoc()"},  
      {"value": "Open", "onclick": "OpenDoc()"},  
      {"value": "Save", "onclick": "SaveDoc()"}  
    ]  
  }
}}
"#;
        let res = from_json(json);
        println!("{:?}", res.join("\n"));
    }
}
