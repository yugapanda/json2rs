use std::env::args;

mod lib;
use lib::from_json;

fn main() {
    let args: Vec<String> = args().collect();

    println!("{}", from_json(&args[1]).join(""));
}
