# JSON RUST Struct CONVERTER
## What's This ?
Simple Converter Json to Rust Struct.

### Example

```
{"menu": {  
  "id": "file",  
  "value": "File",  
  "popup": {  
    "menuitem": [  
      {"value": "New", "onclick": "CreateDoc()"},  
      {"value": "Open", "onclick": "OpenDoc()"},  
      {"value": "Save", "onclick": "SaveDoc()"}  
    ]  
  }
}}
```

to


```
pub struct RootObject {
    menu: Menu,
}

pub struct Menu {
    id: String,
    popup: Popup,
    value: String,
}

pub struct Popup {
    menuitem: Vec<Menuitem>,
}

pub struct Menuitem {
    onclick: String,
    value: String,
}
```

## Usage

```
cargo run '{"menu": { "id": "file", "value": "File", "popup": { "menuitem": [{"value": "New", "onclick": "CreateDoc()"}, {"value": "Open", "onclick": "OpenDoc()"},　{"value": "Save", "onclick": "SaveDoc()"}]}}}'
```

## WASM

Need `wasm-pack`

```
cargo install wasm-pack
```

Compile

```
`wasm-pack build --target web`
```

## Sample Site

```
https://json4rs.hynfias.com/
```
